from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse
from fastapi.exceptions import RequestValidationError
from src.utils import logger
from src.views import view
from pathlib import Path

import logging


def create_app(config_path: Path = Path("configs/logging_config.json")) -> FastAPI:
    logger.CustomizeLogger.make_logger(config_path)

    app = FastAPI(
        debug=False,
        docs_url='/api/docs',
        redoc_url='/api/redoc',
        openapi_url='/api/openapi.json')
    app.logger = logger

    @app.exception_handler(Exception)
    async def unicorn_exception_handler(request: Request, exc: Exception):
        logging.error(exc)
        return JSONResponse(
            status_code=400,
            content={"message": str(exc)},
        )

    @app.exception_handler(RequestValidationError)
    async def validation_exception_handler(request: Request, exc: RequestValidationError):
        logging.error(exc)
        return JSONResponse(
            status_code=422,
            content={"message": str(exc)},
        )

    app.include_router(
        view.router,
        prefix="/api/v1",
        tags=["view"],
    )
    return app
